import argparse
import json
from math import sqrt
import os

from matplotlib import pyplot
import numpy as np
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.header import FDSNNoDataException
from obspy.signal import PPSD
import pandas as pd
import yaml


def read_yaml(path_conf):
    """
    Read the configuration file from the path and return the content.
    """
    with open(path_conf) as conf_yaml:
        conf = yaml.load(conf_yaml)
    return conf


def make_stations_id(path_csv):
    """
    Return a list, each element of the list is a station id.
    """
    stations_id = []
    stations_csv = pd.read_csv(path_csv, dtype={'nework': str, 'station': str,
                               'location_code': str, 'channel': str})
    for _, row in stations_csv.iterrows():
        if row['location_code'] == '__':
            row['location_code'] = ''
        station_id = "{0}.{1}.{2}.{3}".format(row['network'],
                                              row['station'],
                                              row['location_code'],
                                              row['channel'])
        stations_id.append(station_id)
    return stations_id


def extract_stream(station_id, conf_extr):
    """
    Stream is extracted from the FDSN service.
    Return the stream corresponding to the station id.
    """
    url_server = conf_extr['url_server']
    start_time = conf_extr['starttime']
    end_time = conf_extr['endtime']
    client = Client(base_url=url_server)
    st_sta = client.get_waveforms(*station_id.split('.'),
                                  start_time, end_time,
                                  attach_response=True)
    return st_sta


def extract_paz(stream_station):
    """
    Extract poles, zeros, gain and normalization factor from a stream object
    (response is attached in the stream object).
    """
    paz_obj = stream_station[0].stats.response.get_paz()
    paz = {}
    paz['zeros'] = paz_obj.zeros
    paz['poles'] = paz_obj.poles
    paz['sensitivity'] = paz_obj.stage_gain
    paz['gain'] = paz_obj.normalization_factor
    return paz


def compute_ppsd_station(stream_station):
    """
    Compute the probability power spectral density (ppsd) for the station
    Return a dictionnary of ppsd (key is the station name
    and the value the ppsd).
    """
    ppsd_station = PPSD(stream_station[0].stats, extract_paz(stream_station))
    ppsd_station.add(stream_station)
    return ppsd_station


def select_period(ppsd_station, pmin, pmax):
    """
    Return periods between pmin and pmax for which ppsd are computed.
    """
    diff = [abs(period - pmin) for period in ppsd_station.period_bin_centers]
    ind_min = diff.index(min(diff))
    diff = [abs(period - pmax) for period in ppsd_station.period_bin_centers]
    ind_max = diff.index(min(diff))
    period = ppsd_station.period_bin_centers[ind_min:ind_max + 1]
    return period


def compute_th_evt_psd(magnitude, distance, periods, params):
    """
    Return the theoretical psd of event in dB of an event located at a
    certain distance (in meter) of a certain magnitude for different periods.
    params is a dictionnary containing the stress drop in Pa, the S velocity
    close to the receiver in m/s,
    """
    Mo = np.power(10, 1.5 * magnitude + 9)
    stress_drop = params['stress_drop']
    Ro = np.power(7 * Mo / (16 * stress_drop), 1/3)
    vr, vs = (params['s_velocity_receiver'], params['s_velocity_source'])
    ro_r = 1000 * 0.23 * np.power((vr/0.3048), 0.25)
    ro_s = 1000 * 0.23 * np.power((vs/0.3048), 0.25)
    quality_factor = params['quality_factor']
    fc = (2.34 * vs) / (2*np.pi * Ro)
    num_wo = 2 * 2 / np.pi * Mo
    denum_wo = 4 * np.pi * distance * sqrt(ro_r * ro_s * vr * np.power(vs, 5))
    wo = num_wo / denum_wo
    psd_vel_db = []
    for p in periods:
        f = 1/p
        coeff = -(distance * f * np.pi) / (quality_factor * sqrt(vr * vs))
        disp_spec = wo / sqrt(1+np.power(f/fc, 2)) * np.exp(coeff)
        vel_spec_mod = 2*np.pi*f * np.abs(disp_spec)
        psd_vel = np.power(vel_spec_mod, 2) * f * 8 / (sqrt(2) * np.pi)
        psd_vel_db.append(10*np.log10(psd_vel))
    return psd_vel_db


def probability_detection(psd_event, ppsd_noise, periods, detection_threshold):
    """
    Compute the probability of an event to be detected on a period range
    by a station knowing the PSD of the event and the PPSD of the noise at the
    station and setting some threshold.
    """
    ind_pmin = list(ppsd_noise.period_bin_centers).index(periods[0])
    ind_pmax = list(ppsd_noise.period_bin_centers).index(periods[-1])
    psd_noise_seg = []
    for ppsd_noise_seg in ppsd_noise.psd_values:
        psd_noise_seg.append(np.mean(ppsd_noise_seg[ind_pmin: ind_pmax + 1]))
    mean_psd_evt = np.mean(psd_event)
    psd_threshold_evt = mean_psd_evt - detection_threshold['threshold_1']
    # event detected at the station if the noise at the station is smaller
    # than psd_threshold_evt.
    if psd_threshold_evt > max(ppsd_noise.db_bin_edges):
        probability = 1
        return probability
    if psd_threshold_evt < min(ppsd_noise.db_bin_edges):
        probability = 0
        return probability
    bin_evt_detection = np.histogram(psd_threshold_evt,
                                     bins=ppsd_noise.db_bin_edges)[0]
    hist_noise = np.histogram(psd_noise_seg, bins=ppsd_noise.db_bin_edges)[0]
    cdf_noise = np.divide(np.cumsum(hist_noise), len(psd_noise_seg))
    probability = np.sum(np.multiply(bin_evt_detection, cdf_noise))
    return round(probability, 2)


def write_json(result, path_json):
    """
    Store for each station the results of the probability detection
    in a json file.
    """
    with open(path_json, 'w') as fp:
        json.dump(result, fp)


def read_json(path_json):
    """
    Read the json file containing the results.
    """
    with open(path_json, 'r') as fp:
        return json.load(fp)


def process(path_conf, csv_stations, rep_output):
    """
    Compute the probability to detect earthquakes of different magnitudes
    at different distance for different stations.
    """
    epsilon = 1e-10
    conf = read_yaml(path_conf)
    stations_id = make_stations_id(csv_stations)
    pmin, pmax = (1/conf['frequency_range']['fmax'],
                  1/conf['frequency_range']['fmin'])
    dmin, dmax = (conf['distance']['dmin'], conf['distance']['dmax'])
    ml_min, ml_max = (conf['magnitude']['ml_min'], conf['magnitude']['ml_max'])
    distances = np.arange(1000 * dmin, 1000 * dmax + epsilon,
                          1000 * conf['distance']['step'])
    magnitudes = np.arange(ml_min, ml_max + epsilon, conf['magnitude']['step'])
    for sta_id in stations_id:
        sta = sta_id.split('.')[1]
        rep_sta = os.path.join(rep_output, sta)
        os.mkdir(rep_sta)
        try:
            st = extract_stream(sta_id, conf['param_extract_wf'])
        except FDSNNoDataException:
            msg = 'No data for the station {0}'.format(sta)
            print(msg)
            continue
        ppsd_sta = compute_ppsd_station(st)
        periods = select_period(ppsd_sta, pmin, pmax)
        result_per_sta = []
        for m in magnitudes:
            for d in distances:
                th_evt_psd = compute_th_evt_psd(m, d, periods,
                                                conf['param_th_evt_psd'])
                prob = probability_detection(th_evt_psd, ppsd_sta, periods,
                                             conf['threshold'])
                result_per_sta.append((m, d/1000, prob))
        path_json = os.path.join(rep_sta, '{0}.json'.format(sta))
        write_json(result_per_sta, path_json)


def feed_dict(dictionnary, magnitude, distance, probability):
    """
    Feed the dictionnary.
    """
    dictionnary[str(magnitude)][0].append(distance)
    dictionnary[str(magnitude)][1].append(probability)


def parse_data(raw_result):
    """
    This function orders the data so as to obtain a dictionnary
    with key is the magnitude and the value is a list. The first element
    of the list contains the list of the distance between the event and the
    station and the second element the list of the probability of detection.
    """
    result_parsed = {}
    for elem in raw_result:
        magnitude, distance, prob = (elem[0], elem[1], elem[2])
        try:
            feed_dict(result_parsed, magnitude, distance, prob)
        except KeyError:
            result_parsed[str(magnitude)] = [[], []]
            feed_dict(result_parsed, magnitude, distance, prob)
    return result_parsed


def graph_per_sta(results_per_sta, sta, rep_output):
    """
    For each station, plot the detection probability in function of the
    distance for different magnitudes.
    """
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, len(results_per_sta.keys()),
                              dtype=np.int16)
    ind = 0
    for mag, val in results_per_sta.items():
        distance, prob = val[0], val[1]
        pyplot.plot(distance, prob, color=cmap(index_color[ind]), linewidth=2,
                    label='Mw=' + mag)
        pyplot.legend(loc='best')
        ind += 1
    pyplot.xlabel('distance to the event (Km)')
    pyplot.ylabel('probability of detection (%)')
    figure_name = 'detection_' + sta + '.png'
    path_figure = os.path.join(rep_output, sta, figure_name)
    pyplot.savefig(path_figure, dpi=300)
    pyplot.cla()
    pyplot.clf()


def graph_all_sta(results_all_sta, rep_output):
    """
    For each magnitude, plot the detection probability in function of the
    distance for all the stations.
    """
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, len(results_all_sta.keys()),
                              dtype=np.int16)
    magnitudes = set(results_all_sta[list(results_all_sta.keys())[0]])
    for mag in magnitudes:
        ind = 0
        for sta in results_all_sta.keys():
            distance = results_all_sta[sta][mag][0]
            prob = results_all_sta[sta][mag][1]
            pyplot.plot(distance, prob, color=cmap(index_color[ind]),
                        linewidth=2, label=sta)
            ind += 1
        pyplot.xlabel('distance to the event (Km)')
        pyplot.ylabel('probability of detection (%)')
        pyplot.legend(loc='best')
        figure_name = 'detection_event_Mw=' + mag + '.png'
        path_figure = os.path.join(rep_output, figure_name)
        pyplot.savefig(path_figure, dpi=300)
        pyplot.cla()
        pyplot.clf()


def plot_graphs(rep_output):
    """
    Plot 2 types of graphs:
        - for each station, plot the detection probability in function of the
        distance for different magnitudes.
        - for each magnitude, plot the detection probability in function of the
        distance for all the stations.
    """
    results_all_sta = {}
    for sta in os.listdir(rep_output):
        json_file = sta + '.json'
        path_json_file = os.path.join(rep_output, sta, json_file)
        results_per_sta = parse_data(read_json(path_json_file))
        results_all_sta[sta] = results_per_sta
        graph_per_sta(results_per_sta, sta, rep_output)
    graph_all_sta(results_all_sta, rep_output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute the probability\
                                     to detect earthquake of different\
                                     magnitudes at different distance')
    parser.add_argument('conf_file', help='path configuration file\
                        in yaml format')
    parser.add_argument('csv_stations', help='path csv file containing the\
                        stations id')
    parser.add_argument('rep_output', help='path directory where a directory \
                        will be created to store the results')
    args = parser.parse_args()
    path_conf = args.conf_file
    csv_stations = args.csv_stations
    rep_output = args.rep_output
    path_dir_result = os.path.join(rep_output, 'results')
    os.mkdir(path_dir_result)
    process(path_conf, csv_stations, path_dir_result)
    plot_graphs(path_dir_result)
